module.exports = function(grunt) {

  var PublicJS = './public/js/'
    , JQueryJS = './coffee/out/jquery/'
    , CoffeeModel = './coffee/model/'
    , CoffeeView = './coffee/view/'
    , CoffeeCollection = './coffee/collection/'
    , CoffeeApp = './coffee/app/';

  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    coffee: {
      options: {
        join: true,
        bare: true
      },

      /*
        Device page Backbone structure
      *
      compileDevice: {
        files: {
          './coffee/out/jquery/device.js': [
            CoffeeModel + 'device-model.coffee',
            CoffeeView + 'device-view.coffee',
            CoffeeApp + 'device-app.coffee'
          ]
        }
      },

      /*
        Devices page Backbone structure
      *
      compileDevices: {
        files: {
          './coffee/out/jquery/devices.js': [
            CoffeeModel + 'device-model.coffee',
            CoffeeCollection + 'devices-collection.coffee',
            CoffeeView + 'devices-view.coffee',
            CoffeeApp + 'devices-app.coffee'
          ]
        }
      },
      */
      compileAngular: {
        files: {
          './public/js/angular-app.js': [
            './coffee/angular/app.coffee',
            './coffee/angular/controllers/devicesController.coffee',
            './coffee/angular/controllers/deviceController.coffee',
            './coffee/angular/controllers/deviceTemperatureController.coffee',
            './coffee/angular/controllers/deviceTemperatureDataController.coffee',
            './coffee/angular/controllers/newDeviceController.coffee',
            './coffee/angular/controllers/newDeviceModalController.coffee',
            './coffee/angular/controllers/editDeviceModalController.coffee',
            './coffee/angular/directives/directives.coffee'
          ]
        }
      }
    },

    wrap: {
      jquery: {
        cwd: JQueryJS,
        src: ['*.js'],
        dest: PublicJS,
        expand: true,
        options: {
          indent: '  ',
          wrapper: ['$(function() {\n', '\n});']
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-wrap');

  grunt.registerTask('default', ['coffee', 'wrap']);
}