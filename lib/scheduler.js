/*
  Dependencies
*/
var cronJob = require('cron').CronJob
  , http = require('http');

/*
  Export the constructor
 */
exports = module.exports = Scheduler;

/*
  Schedule constructor.
 */
function Scheduler (_pattern) {
  this.schedulePattern = _pattern;
  this.autoStart = true;
  this.job = undefined;
};

Scheduler.prototype.start = function () {

  var self = this;

  self.job = new cronJob(
      self.schedulePattern
    , self.jobStarted
    , self.jobFinished
    , self.autoStart
  );

  console.info('New scheduled created. AutoStart = ', self.autoStart);
};

/**
 *  Scheduled job that queries the temperature and humidity data form
 *  Arduino units.
 *
 *  If unit is unresponsive and the query results in error, the device
 *  record will be updated with 'status: error'.
 */
Scheduler.prototype.jobStarted = function () {
  console.info('Scheduling run started');

  Device.find(function (err, devices) {
    devices.forEach(function (device) {

      http.get({
          host: device.ip,
          port: device.port,
          path: '/getTemps'       // Toistaiseksi kovakoodattuna, tämän vois tehdä toisinkin
        }
        , function (res) {

          var data = "";

          /* Collect all packages of data */
          res.on('data', function (chunk) {
            data += chunk;
          });

          /* Process once everything is received */
          res.on('end', function () {
            console.log('Received : %s', data);

            try {
              var dataJSON = JSON.parse(data);
            } catch (e) {
              console.error('Parse error: ', e);
            }

            if (dataJSON.temperature != undefined && dataJSON.humidity != undefined) {
              Temperature.create({
                  temperature: dataJSON.temperature,
                  humidity: dataJSON.humidity,
                  deviceId: device._id
                }, function (err, doc) {
                  if (err) {
                    console.error(err);
                  } else {
                    if (device.status != 'active') {
                      Device.findByIdAndUpdate(device._id, { status: 'active', errorDesc: "" }, function (err, doc) {
                        if (doc) {
                          doc.save();
                        }
                      });
                    }
                  }
              });
            }
          });
        }
      ).on('error', function (e) {
        console.log("Got error: " + JSON.stringify(e));

        // Update Device's state to error
        Device.findByIdAndUpdate(device._id, { status: 'error', errorKey: e.code }, function (err, doc) {
          if (doc) {
            doc.save();
          }
        });
      });

    });
  });

}

Scheduler.prototype.jobFinished = function () {
  console.info('Scheduling job stopped');
}