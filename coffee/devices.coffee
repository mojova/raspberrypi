class ViewConstructor
  constructor: (@data) ->
    source = $('#device-rows-template').html()
    template = Handlebars.compile(source)

    for key, rows of @data
      #alert JSON.stringify rows
      if rows.length > 0
        content = template items: rows
        $('#devices-' + key + '-body').html content
        $('#devices-' + key).addClass('show').removeClass('hide')
      else
        $('#devices-' + key + '-body').html ''
        $('#devices-' + key).addClass('hide').removeClass('show')

  getData: (id) ->
    console.debug 'data for %s', id
    for key, rows of @data
      for row in rows
        return row if row._id == id

$(document).ready ->

  $newDeviceModal = $('#new-device-modal')

  socket = io.connect()

  socket.on('devices'
    (data) ->
      window.viewObj = new ViewConstructor data
  )

  $submitNewDevice = $('#submit-new-device-btn')
  $submitNewDevice.button()
  $submitNewDevice.on('click'
   (e) ->
    if checkInputs()

      # Set Bootstrap button loading state
      $submitNewDevice.button 'loading'

      newData =
        _id: $('#new-device-id').val()
        name: $('#new-device-name').val()
        ip: $('#new-device-ip').val()
        port: $('#new-device-port').val()

      socket.emit('upsertDevice', newData
        (err) ->
          $submitNewDevice.button 'reset'
          $newDeviceModal.modal 'hide'
      )
    else
      alert 'Fill all fields!'
  )

  # Needed a global because listeners were not hitting the Bootstrap dropdown achors...
  window.removeDeviceById = (id) ->
    if confirm 'Confirm to delete device entry'
      socket.emit('removeDevice', id
        (err) ->
          if err
            alert(err)
      )

  # Needed a global because listeners were not hitting the Bootstrap dropdown achors...
  window.editDeviceById = (id) ->
    #alert 'edit: '+id

    # Set values to Modal popup
    data = window.viewObj.getData id

    if data
      $('#new-device-id').val data._id
      $('#new-device-name').val data.name
      $('#new-device-ip').val data.ip
      $('#new-device-port').val data.port

      $newDeviceModal.modal 'show'

  # Clear modal fields once hidden
  $newDeviceModal.on('hidden.bs.modal'
    (e) ->
      $('#new-device-id').val ''
      $('#new-device-form')[0].reset()
  )

  # Trigger the loading of Andruino entries
  socket.emit 'fetchDevices'

checkInputs = ->
  return false if $('#new-device-name').val() == ''
  return false if $('#new-device-ip').val() == ''
  return false if $('#new-device-port').val() == ''
  true