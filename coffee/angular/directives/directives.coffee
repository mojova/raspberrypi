automationApp.directive "btnLoading", ->
  ($scope, element, attrs) -> (
    $scope.$watch ->
      $scope.$eval attrs.btnLoading
    ,(loading) ->
      if loading
        return element.button('loading')
    element.button('reset')
  )