automationApp = angular.module 'automationApp', ['ngRoute', 'ngSocket', 'ui.bootstrap']

automationApp.config ($routeProvider) ->
  $routeProvider
    .when '/',
      controller: 'DevicesController'
      templateUrl: '/tmpl/index'
    .when '/device/:deviceID',
      controller: 'DeviceController',
      templateUrl: '/tmpl/device'
    .otherwise
      redirectTo: '/'