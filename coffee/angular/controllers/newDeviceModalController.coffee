automationApp.controller 'NewDeviceModalController', ($scope, $socket, $modalInstance) ->

  $scope.loading = false
  $scope.action = 'addNew'

  $scope.device =
    name: ''
    ip: ''
    port: ''

  $scope.save = ->

    console.log 'Creating device with ', $scope.device

    if $scope.device.ip != '' and $scope.device.port != '' and $scope.device.name != ''
      $scope.loading = true

      data =
        _id: '',
        name: $scope.device.name,
        ip: $scope.device.ip,
        port: $scope.device.port

      $socket.emit('device:upsert', data
        (err) ->
          $modalInstance.dismiss 'cancel'
      )

  $scope.cancel = ->
    $modalInstance.dismiss 'cancel'