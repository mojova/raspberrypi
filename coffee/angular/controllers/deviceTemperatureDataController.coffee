automationApp.controller 'DeviceTemperatureDataController', ($scope, $socket) ->

  init = ->
    $socket.emit 'temperatures:getWeek', id: $scope.deviceID

  init()

  $socket.on 'temperatures:week', (data) ->
    console.log("Temperature data for week: ", data);