automationApp.controller 'DeviceController', ($scope, $socket, $routeParams, $location, $modal) ->

  $scope.deviceID = $routeParams.deviceID

  # Trigger initialization
  # It will emit get via socket and server will emit all devices
  # to a event listener attached and update the device list accordingly.
  init = ->
    $socket.emit( 'device:getById', $scope.deviceID,
      (err) ->
        $location.path('/') if err?
    )

  init()

  $socket.on 'device', (data) ->
    console.log 'received device data', data
    $scope.device = data

  $socket.on 'temperature', (data) ->
    console.log 'received temperature data', data
    $scope.realTimeData = data

  $scope.remove = ->
    console.log 'Remove device data', $scope.device
    if confirm 'Confirm to delete device entry'
      $socket.emit( 'device:remove', $scope.device._id,
        (err) ->
          $location.path '/'
      )

  $scope.edit = ->
    modalInstance = $modal.open
      templateUrl: 'deviceDataModal.html',
      controller: 'EditDeviceModalController',
      resolve:
        data: ->
          _id : $scope.device._id,
          name: $scope.device.name,
          ip: $scope.device.ip,
          port: $scope.device.port

  # Destroy bindings for this controller when view is changed
  $scope.$on '$destroy', (event) ->
    $socket.removeAllListeners()