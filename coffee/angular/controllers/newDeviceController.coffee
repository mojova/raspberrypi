automationApp.controller 'NewDeviceController', ($scope, $modal) ->

  $scope.open = ->
    modalInstance = $modal.open
      templateUrl: 'deviceDataModal.html',
      controller: 'NewDeviceModalController'