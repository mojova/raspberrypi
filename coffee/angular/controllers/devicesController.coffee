automationApp.controller 'DevicesController', ($scope, $socket) ->

  # Trigger initialization
  # It will emit get via socket and server will emit all devices
  # to a event listener attached and update the device list accordingly.
  init = ->
    $socket.emit 'devices:get'

  init()

  $socket.on 'devices:all', (data) ->
    console.log 'received device data', data
    $scope.devices = data;

  # When changes are emitted from server side this forces the view to
  # render according to the new state
  $socket.on 'devices:broadcast', (data) ->
    $scope.devices = data;

  # Destroy bindings for this controller when view is changed
  $scope.$on '$destroy', (event) ->
        $socket.removeAllListeners()