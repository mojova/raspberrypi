// Generated by CoffeeScript 1.6.3
automationApp.controller('EditDeviceModalController', function($scope, $socket, $modalInstance, data) {
  console.log(data);
  $scope.action = 'edit';
  $scope.device = data;
  $scope.save = function() {
    console.log('save');
    if ($scope.device.ip !== '' && $scope.device.port !== '' && $scope.device.name !== '') {
      $scope.loading = true;
    }
    data = {
      _id: $scope.device._id,
      name: $scope.device.name,
      ip: $scope.device.ip,
      port: $scope.device.port
    };
    return $socket.emit('device:upsert', data, function(err) {
      $socket.emit('device:getById', $scope.device._id, function(err) {
        if (err != null) {
          $location.path('/');
        }
        return $modalInstance.dismiss('cancel');
      });
      return $modalInstance.dismiss('cancel');
    });
  };
  return $scope.cancel = function() {
    return $modalInstance.dismiss('cancel');
  };
});
