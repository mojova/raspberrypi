automationApp.controller 'EditDeviceModalController', ($scope, $socket, $modalInstance, data) ->

  console.log data

  $scope.action = 'edit'
  $scope.device = data

  $scope.save = ->
    console.log 'save'

    if $scope.device.ip != '' and $scope.device.port != '' and $scope.device.name != ''
      $scope.loading = true

    data =
        _id: $scope.device._id,
        name: $scope.device.name,
        ip: $scope.device.ip,
        port: $scope.device.port

      $socket.emit('device:upsert', data
        (err) ->
          $socket.emit( 'device:getById', $scope.device._id,
            (err) ->
              $location.path('/') if err?
              $modalInstance.dismiss 'cancel'
          )
          $modalInstance.dismiss 'cancel'
      )


  $scope.cancel = ->
    $modalInstance.dismiss 'cancel'