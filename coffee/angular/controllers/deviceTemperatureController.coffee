automationApp.controller 'DeviceTemperatureController', ($scope, $socket) ->

  $scope.deviceID

  init = ->
    $socket.emit 'temperature:initDummy', id: $scope.deviceID

  init()

  $socket.on 'temperature:data', (data) ->
    console.log 'received temperature data', data
    $scope.realTimeData = data;

  # Destroy bindings for this controller when view is changed
  $scope.$on '$destroy', (event) ->
    $socket.removeAllListeners()