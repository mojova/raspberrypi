
/**
 * Module dependencies.
 */

var express = require('express')
  , http = require('http')
  , path = require('path')
  , i18n = require('i18next')
  , swig = require('swig')
  , Scheduler = require('./lib/scheduler')
  , socket = require('./routes/sockets')
  , template = require('./routes/templates')
  , routes = require('./routes')
  , mongoInit = require('./mongodb/setup');

/*
  Global for socket etc. usage
 */
mongoose = require('mongoose').connect('mongodb://localhost/app');

/*
  Init express app
 */
var app = express();

/*
  Create database connection
 */
var db = mongoose.connection;

/*
  Bind database connection events.
 */
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', mongoInit);

/*
  Init localization. For fi-FI see ./locales/fi-FI/translation.json
 */
i18n.init();
i18n.registerAppHelper(app);

app.engine('html', swig.renderFile)

app.configure(function(){
  app.use(i18n.handle);
  app.set('port', process.env.PORT || 8080);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'html');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(
    path.join(__dirname, 'public'))
  );
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

/*
  Routes
 */
app.get('/', routes.devices);
//app.get('/device/:deviceId', routes.device);
app.get('/tmpl/index', template.index);
app.get('/tmpl/device', template.device);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

/*
  Socket.io initialization
 */
io = require('socket.io').listen(server);
io.sockets.on('connection', socket);

/*
  Create a scheduler job once the schemas have been defined
 */
TemperatureScheduler = new Scheduler('0 0 * * * *').start();