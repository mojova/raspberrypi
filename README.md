# Mojova Home Automation

Application server which provides monitor to browse room temperatures from n number of Arduino powered monitors. Temperatures are fetched in predefined time intervals and stored to database.

## Tech stack

New implementation with a full JavaScript stack.

### Backend

JavaScript based components with Node and NoSQL solution with MongoDB.

  * [NodeJS](http://nodejs.org/)
  * [MongoDB](http://www.mongodb.org/)

### Frontend

Styling and responsiveness is implemented using:

  * [Twitter Bootstrap 3](http://getbootstrap.com/)

Possible frontend frameworks to be implemented:

  * [Backbone](http://backbonejs.org/)
  * [Angular](http://angularjs.org/)
  * [EmberJS](http://emberjs.com/)