var mongoose = require('mongoose');

module.exports = function () {

  console.info('MongoDB connection opened successfully');

  var deviceSchema = new mongoose.Schema({
    name: String,
    ip: String,
    port: String,
    status: {type: String, default: 'active'},
    errorKey: String,
    data: [{}]
  });

  Device = mongoose.model('Device', deviceSchema);

  var temperatureSchema = new mongoose.Schema({
    date: { type: Date, default: Date.now},
    temperature: String,
    humidity: String,
    deviceId: {type: mongoose.Schema.ObjectId, ref: 'Device'}
  });

  Temperature = mongoose.model('Temperature', temperatureSchema);
}