
/*

 Require all necessary routes from ./routes/*

 from: http://stackoverflow.com/questions/9027648/proper-way-to-organize-myapp-routes


  var fs = require('fs');

  module.exports = function(app) {

    var routePath = __dirname + '/';
    console.log('Loading modules from ' + routePath);

    fs.readdirSync(routePath).forEach(function(file) {

        // Cut the .js extension from the route files
        if (file != 'index.js' && file.indexOf('.js') != -1) {
            var route = routePath + file.substr(0, file.indexOf('.'));
            console.log('Loading route ' + route);
            require(route)(app);
        }
    });
}
*/

var i18n = require('i18next');

exports.devices = function (req, res) {

  res.render('layout', {
    title: i18n.t('app.name')
  });

  /*
  Device.find(null, null, { sort: 'name' }, function (err, set) {
    res.render('index', {
      devices: set,
      title: i18n.t('app.name')
    });
  });
*/
}

exports.device = function (req, res) {
  Device.findById(req.params.deviceId, function (err, doc) {
    if (err) console.error('Error fetching device ', err);

    if (doc) {
      res.render('device', {
        device: doc
      });
    } else {
      console.warn('Cannot find device with id ', req.params.deviceId);
      res.redirect(404, '/');
    }
  });
}