/*
 *  Deliver templates to AngularJS. No static html since we need to include
 *  the localization values
 */

var i18n = require('i18next');

exports.index = function (req, res) {
  res.render('tmpl/index', {
    title: i18n.t('app.name')
  });
}

exports.device = function (req, res) {
  res.render('tmpl/device', {
    title: i18n.t('app.name')
  });
}