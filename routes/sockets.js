var format = require('format');

module.exports = function (socket) {

  socket.on('device:remove', function (id, fn) {

    console.log(format('Removing %s', id));

    Device.findById(id, function (err, doc) {
      if (err) {
        console.error(err);
        fn(err);
      }
      else {
        console.info('Device id %s found, proceeding to delete', id);
        doc.remove(function (err) {
          if (err) {
            console.error(err);
            fn(err);
          }
          else {
            fn(null);
            // Now tell all the sockets of the new device list
            deviceList(null, function (devices) {
              io.sockets.emit('devices:broadcast', devices);
            })
          }
        });
      }
    });
  });

  socket.on('device:upsert', function (data, fn) {
    var _id = data._id;

    console.log(format('Adding %s', JSON.stringify(data)));

    if (_id === "" || _id == null || _id === undefined) {
      _id = mongoose.Types.ObjectId();
    }

    Device.findByIdAndUpdate(_id, {
      name: data.name,
      ip: data.ip,
      port: data.port,
      status: 'active'
    }, {
      upsert: true
    }, function (err, _device) {
      fn(err);
      if (err) {
        console.error(err);
      } else {
        console.info('Device id %s found and updated successfully', _id);
        // Now tell all the sockets of the new device list
        deviceList(null, function (devices) {
          io.sockets.emit('devices:broadcast', devices);
        });
      }
    });
  });

  socket.on('device:getById', function (id, fn) {

    console.log(id);

    Device.findById(id, function (err, doc) {
      if (err) {
        console.error(err);
        fn(err);
      }
      else {
        socket.emit('device', doc);
      }
    });
  });

  socket.on('devices:get', function (data) {
    deviceList(data, function (devices) {
      socket.emit('devices:all', devices);
    });
  });

/*
  socket.on('devices:broadcast', function (data) {
    deviceList(data, function(devices) {
      io.sockets.emit('devices:broadcast', devices);
    });
  });
*/

  socket.on('temperature:initDummyByIp', function (data) {

  });

  socket.on('temperatures:getWeek', function (data) {

    console.log("Viikon eka päivä", new Date());

  });
};

var deviceList = function (data, fn) {

  Device.find(function (err, devices) {

  /*
  var statusActive = statusError = [];

  devices.forEach(function (device) {
    switch (device.status) {
      case 'active':
        statusActive.push(device);
        break;
      default:
        statusError.push(device);
    }
  });
*/

    fn(devices);
  });
}