$(function() {

  var Device, DeviceList, Devices, DevicesAppView, DevicesView;
  
  Device = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      name: 'no name unit',
      ip: '192.168.1.0',
      port: '80',
      status: 'active',
      data: {}
    }
  });
  
  DeviceList = Backbone.Collection.extend({
    model: Device,
    error: function() {
      return this.where({
        status: 'error'
      });
    },
    active: function() {
      return this.where({
        status: 'active'
      });
    }
  });
  
  DevicesView = Backbone.View.extend;
  
  DevicesAppView = Backbone.View.extend;
  
  Devices = new DevicesAppView;
  

});