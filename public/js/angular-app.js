var automationApp;

automationApp = angular.module('automationApp', ['ngRoute', 'ngSocket', 'ui.bootstrap']);

automationApp.config(function($routeProvider) {
  return $routeProvider.when('/', {
    controller: 'DevicesController',
    templateUrl: '/tmpl/index'
  }).when('/device/:deviceID', {
    controller: 'DeviceController',
    templateUrl: '/tmpl/device'
  }).otherwise({
    redirectTo: '/'
  });
});

automationApp.controller('DevicesController', function($scope, $socket) {
  var init;
  init = function() {
    return $socket.emit('devices:get');
  };
  init();
  $socket.on('devices:all', function(data) {
    console.log('received device data', data);
    return $scope.devices = data;
  });
  $socket.on('devices:broadcast', function(data) {
    return $scope.devices = data;
  });
  return $scope.$on('$destroy', function(event) {
    return $socket.removeAllListeners();
  });
});

automationApp.controller('DeviceController', function($scope, $socket, $routeParams, $location, $modal) {
  var init;
  $scope.deviceID = $routeParams.deviceID;
  init = function() {
    return $socket.emit('device:getById', $scope.deviceID, function(err) {
      if (err != null) {
        return $location.path('/');
      }
    });
  };
  init();
  $socket.on('device', function(data) {
    console.log('received device data', data);
    return $scope.device = data;
  });
  $socket.on('temperature', function(data) {
    console.log('received temperature data', data);
    return $scope.realTimeData = data;
  });
  $scope.remove = function() {
    console.log('Remove device data', $scope.device);
    if (confirm('Confirm to delete device entry')) {
      return $socket.emit('device:remove', $scope.device._id, function(err) {
        return $location.path('/');
      });
    }
  };
  $scope.edit = function() {
    var modalInstance;
    return modalInstance = $modal.open({
      templateUrl: 'deviceDataModal.html',
      controller: 'EditDeviceModalController',
      resolve: {
        data: function() {
          return {
            _id: $scope.device._id,
            name: $scope.device.name,
            ip: $scope.device.ip,
            port: $scope.device.port
          };
        }
      }
    });
  };
  return $scope.$on('$destroy', function(event) {
    return $socket.removeAllListeners();
  });
});

automationApp.controller('DeviceTemperatureController', function($scope, $socket) {
  var init;
  $scope.deviceID;
  init = function() {
    return $socket.emit('temperature:initDummy', {
      id: $scope.deviceID
    });
  };
  init();
  $socket.on('temperature:data', function(data) {
    console.log('received temperature data', data);
    return $scope.realTimeData = data;
  });
  return $scope.$on('$destroy', function(event) {
    return $socket.removeAllListeners();
  });
});

automationApp.controller('DeviceTemperatureDataController', function($scope, $socket) {
  var init;
  init = function() {
    return $socket.emit('temperatures:getWeek', {
      id: $scope.deviceID
    });
  };
  init();
  return $socket.on('temperatures:week', function(data) {
    return console.log("Temperature data for week: ", data);
  });
});

automationApp.controller('NewDeviceController', function($scope, $modal) {
  return $scope.open = function() {
    var modalInstance;
    return modalInstance = $modal.open({
      templateUrl: 'deviceDataModal.html',
      controller: 'NewDeviceModalController'
    });
  };
});

automationApp.controller('NewDeviceModalController', function($scope, $socket, $modalInstance) {
  $scope.loading = false;
  $scope.action = 'addNew';
  $scope.device = {
    name: '',
    ip: '',
    port: ''
  };
  $scope.save = function() {
    var data;
    console.log('Creating device with ', $scope.device);
    if ($scope.device.ip !== '' && $scope.device.port !== '' && $scope.device.name !== '') {
      $scope.loading = true;
      data = {
        _id: '',
        name: $scope.device.name,
        ip: $scope.device.ip,
        port: $scope.device.port
      };
      return $socket.emit('device:upsert', data, function(err) {
        return $modalInstance.dismiss('cancel');
      });
    }
  };
  return $scope.cancel = function() {
    return $modalInstance.dismiss('cancel');
  };
});

automationApp.controller('EditDeviceModalController', function($scope, $socket, $modalInstance, data) {
  console.log(data);
  $scope.action = 'edit';
  $scope.device = data;
  $scope.save = function() {
    console.log('save');
    if ($scope.device.ip !== '' && $scope.device.port !== '' && $scope.device.name !== '') {
      $scope.loading = true;
    }
    data = {
      _id: $scope.device._id,
      name: $scope.device.name,
      ip: $scope.device.ip,
      port: $scope.device.port
    };
    return $socket.emit('device:upsert', data, function(err) {
      $socket.emit('device:getById', $scope.device._id, function(err) {
        if (err != null) {
          $location.path('/');
        }
        return $modalInstance.dismiss('cancel');
      });
      return $modalInstance.dismiss('cancel');
    });
  };
  return $scope.cancel = function() {
    return $modalInstance.dismiss('cancel');
  };
});

automationApp.directive("btnLoading", function() {
  return function($scope, element, attrs) {
    $scope.$watch(function() {
      return $scope.$eval(attrs.btnLoading);
    }, function(loading) {
      if (loading) {
        return element.button('loading');
      }
    });
    return element.button('reset');
  };
});
