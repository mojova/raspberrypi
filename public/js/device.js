$(function() {

  var Device, DeviceAppView, DeviceView;
  
  Device = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      name: 'no name unit',
      ip: '192.168.1.0',
      port: '80',
      status: 'active',
      data: {}
    }
  });
  
  DeviceView = Backbone.View.extend({
    template: '',
    initialize: function() {
      this.model.on('change', this.render, this);
      return this.model.on('destroy', this.remove, this);
    },
    events: {
      "click a": ''
    },
    render: function() {
      return this.$el.html(this.template(this.model.attributes));
    }
  });
  
  DeviceAppView = Backbone.View.extend;
  
  Device = new DeviceAppView;
  

});